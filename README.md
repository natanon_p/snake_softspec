# READ ME #

### How many class in this project? ###
* GameActivity
* Die
* Game
* Piece
* Player
* BoardView

### Creator ###
* GameActivity creates Game and BoardView.
* Player creates Piece.
* Game creates a list of Player and Die.

### Controller ###
* GameActivity control the boardView to create and update the UI of the game when Player take turn and when the game restart.
* boardView control the changing in the UI when GameActivity is updated.